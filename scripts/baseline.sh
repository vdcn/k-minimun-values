#!/bin/bash
#

##.EPS=(0.4 0.3 0.2)
##DELTA=(0.1 0.05 0.01)
TARGET=(0 1 2 3 4 5 6 7 8)
##
echo "id,target,epsilon,delta,tempo_inicio,tempo_fim,memoria,prediction" > hll_baseline.csv

for t in "${TARGET[@]}";
do
    echo $t
    t1=`(date)`
    mem_pred=`(scala -J-Xms4096m -J-Xmx4096m ./target/scala-2.13/hll-assembly-0.2.jar -t $t -e 0 -d 0 "data/network_flows.csv")`
    t2=`(date)`
    echo "$id,$t,0,0,$t1,$t2,$mem_pred" >> hll_baseline.csv
    sleep 1
done
