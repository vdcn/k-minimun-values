#!/bin/bash
#

##.EPS=(0.4 0.3 0.2)
##DELTA=(0.1 0.05 0.01)
##TARGET=(0 1 2 3 4 5 6 7 8)
##
echo "id,target,epsilon,delta,tempo_inicio,tempo_fim,memoria,prediction" > hll_trace.csv

while IFS=, read -r id t e d0;
do
    d=`echo $d0 | sed 's/\\r//g'`
    t1=`(date)`
    mem_pred=`(scala -J-Xms4096m -J-Xmx4096m ./target/scala-2.13/hll-assembly-0.2.jar -t $t -e $e -d $d "data/network_flows.csv")`
    t2=`(date)`
    echo "$id,$t,$e,$d,$t1,$t2,$mem_pred" >> hll_trace.csv
    sleep 1

done < data/executions.csv



#for t in "${TARGET[@]}";
#do
#    for e in "${EPS[@]}";
#    do
#        for d in "${DELTA[@]}";
#        do
#            scala -J-Xms4096m -J-Xmx4096m ./target/scala-2.13/hll-assembly-0.2.jar -t $t -e $e -d $d "../data/network_flows.csv"
#            sleep 1
#        done
#    done
#done
