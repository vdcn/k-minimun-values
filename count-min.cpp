#include<bits/stdc++.h>
#include <climits>
#include <cmath>
#include <cstdint>
//-----------------------------------------------------------------------------
// MurmurHash3 was written by Austin Appleby, and is placed in the public
// domain. The author hereby disclaims copyright to this source code.

// Note - The x86 and x64 versions do _not_ produce the same results, as the
// algorithms are optimized for their respective platforms. You can still
// compile and run any of them on any platform, but your performance with the
// non-native version will be less than optimal.

#include "MurmurHash3.h"

//-----------------------------------------------------------------------------
// Platform-specific functions and macros

// Microsoft Visual Studio

#if defined(_MSC_VER)

#define FORCE_INLINE	__forceinline

#include <stdlib.h>

#define ROTL32(x,y)	_rotl(x,y)
#define ROTL64(x,y)	_rotl64(x,y)

#define BIG_CONSTANT(x) (x)

// Other compilers

#else	// defined(_MSC_VER)

#define	FORCE_INLINE inline __attribute__((always_inline))

inline uint32_t rotl32 ( uint32_t x, int8_t r )
{
  return (x << r) | (x >> (32 - r));
}

inline uint64_t rotl64 ( uint64_t x, int8_t r )
{
  return (x << r) | (x >> (64 - r));
}

#define	ROTL32(x,y)	rotl32(x,y)
#define ROTL64(x,y)	rotl64(x,y)
#define MAX_INT 4294967296

#define BIG_CONSTANT(x) (x##LLU)

#endif // !defined(_MSC_VER)

//-----------------------------------------------------------------------------
// Block read - if your platform needs to do endian-swapping or can only
// handle aligned reads, do the conversion here

FORCE_INLINE uint32_t getblock32 ( const uint32_t * p, int i )
{
  return p[i];
}
//-----------------------------------------------------------------------------
// Finalization mix - force all bits of a hash block to avalanche

FORCE_INLINE uint32_t fmix32 ( uint32_t h )
{
  h ^= h >> 16;
  h *= 0x85ebca6b;
  h ^= h >> 13;
  h *= 0xc2b2ae35;
  h ^= h >> 16;

  return h;
}

//-----------------------------------------------------------------------------

void MurmurHash3_x86_32 ( const void * key, int len,
                          uint32_t seed, void * out )
{
  const uint8_t * data = (const uint8_t*)key;
  const int nblocks = len / 4;

  uint32_t h1 = seed;

  const uint32_t c1 = 0xcc9e2d51;
  const uint32_t c2 = 0x1b873593;

  //----------
  // body

  const uint32_t * blocks = (const uint32_t *)(data + nblocks*4);

  for(int i = -nblocks; i; i++)
  {
    uint32_t k1 = getblock32(blocks,i);

    k1 *= c1;
    k1 = ROTL32(k1,15);
    k1 *= c2;
    
    h1 ^= k1;
    h1 = ROTL32(h1,13); 
    h1 = h1*5+0xe6546b64;
  }

  //----------
  // tail

  const auto * tail = (const uint8_t*)(data + nblocks*4);

  uint32_t k1 = 0;

  switch(len & 3)
  {
  case 3: k1 ^= tail[2] << 16;
  case 2: k1 ^= tail[1] << 8;
  case 1: k1 ^= tail[0];
          k1 *= c1; k1 = ROTL32(k1,15); k1 *= c2; h1 ^= k1;
  }

  //----------
  // finalization

  h1 ^= len;

  h1 = fmix32(h1);

  *(uint32_t*)out = h1;
}
//-----------------------------------------------------------------------------
using namespace std;

// because of c++ map's ordering, i use the hash as the key
map<uint32_t , long long > L;
long long k;
uint64_t item_quantity = 0;
set<uint32_t> distinct_elem;
uint32_t R = UINT32_MAX; //max int 32BIT

void update(long long input, map<uint32_t , long long > &mp, uint32_t seed) {
  if(mp.size() <= k){
    // get h(x)
    const long long *in = &input;
    uint32_t hash_otpt;

    MurmurHash3_x86_32(in, sizeof(uint32_t), seed, &hash_otpt);

    const long long x = *in;
    const uint32_t value = hash_otpt;
    map<uint32_t, long long >::iterator it;
    // find element already added
    it = mp.find(value);
    if(!mp.empty()) {
      if(it == mp.end()){
        mp[value] = x;
        item_quantity++;
        //cout << value << endl;
      }
    }
    else{
      mp[value] = x;
    }
  }
  else{
    auto it = mp.rbegin();
//    cout << it->first << endl;
    mp.erase(it->first);
  }
}

long long query(map<uint32_t, long long> &mp){
  uint32_t den = mp.rbegin()->first;
  const long long value = (k * R)/den;
  return value;
}

int main(int argc, char** argv) {
    // initialize kmv
    // hash function is murmurhash
    int t;

    string line;
    vector< string > data;
    double const_delta = 0.333333333;
    double  eps;
    double delta;

    string path;
    ios::sync_with_stdio(0);
    for(int i = 0; i < argc; i++) {
      cout << "argv[i]: " << argv[i] << endl;
      if(!strcmp(argv[i],"--eps")) {
        string vlue = argv[i+1];
        eps = stof(vlue);
        i++;
      }
      else if(!strcmp(argv[i],"--delta")) {
        string vlue = argv[i+1];
        delta = stof(vlue);
        i++;
      }
      else if(!strcmp(argv[i], "--target")){
        string vlue = argv[i+1];
        t = stoi(vlue);
        i++;
      }
      else{
        path = argv[i];
      }
    }
    ifstream infile(path);
    cout << eps << ' ' << delta << endl;

    long long n = 4*log(1/delta);

    vector<map<uint32_t , long long >> sketches(n);


    const long long m = R;
    // cout << R << endl;
    // cout << m << endl;
    int field_counter = -1;
    const uint64_t a = (4*m)/(eps*R);
    const uint64_t b = 32/(const_delta*eps*eps);

    cout << a << endl;
    cout << b << endl;

    k = max(a, b);


    // reset cursor to the start

    //read header file
    random_device rd;
    int seed = rd();
    seed = seed < 0 ? -seed : seed;
    cout << "my seed: " << seed << endl;
    getline(infile, line);

    while (getline(infile, line, ',')) {
      istringstream iss(line);
      string lineString = iss.str();
      field_counter++;

      if (field_counter == t) {
        field_counter = -1;
        if (lineString == "0") continue;
        long long line_long = stol(lineString);
        distinct_elem.insert(line_long);
        for(int idx = 0; idx < n; idx++){
          update(line_long, sketches[idx], seed + idx);
        }
      }
    }
    vector<long long> median_trick;
//    cout << "query " << query() << endl;

    median_trick.reserve(n);

    for(int idx = 0; idx < n; idx++){
      median_trick.push_back(query(sketches[idx]));
      cout << median_trick[idx] << endl;
    }
    cout << "known elements "<< distinct_elem.size() << endl;
    cout << "median_trick: " << median_trick[n/2] << endl;
    cout << "error: " << distinct_elem.size()/median_trick[n/2] * 100 << "%"<< endl;
}